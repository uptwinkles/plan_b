# Plan_b

Plan_b is a chatbot for Riot which obliterates a chatroom one has admin access to.
The problem is, a Riot chatroom with multiple admins means that one admin cannot remove the other.
Therefore, there is no way for a single admin to take down the chatroom.

To use it, invite the chatbot into the room and make it an admin.
It will then kick everyone in the room on every sync, and redact every message that gets posted.
This prevents other admins from restoring the room simply by inviting kicked users.

A whitelist can be configured so that certain users may be unaffected.

## Usage

At the time of writing, `@plan_b:matrix.org` is online.
Simply invite her to the room and make her an admin, and it will "just work."

To deploy an instance yourself, define these variables in your environment:

| Variable              | Description                      | Example                                               |
|-----------------------|----------------------------------|-------------------------------------------------------|
| MATRIX_USER           | Matrix username                  | @example:matrix.org                                   |
| MATRIX_SERVER_URL     | Matrix homeserver URL            | https://matrix.org                                    |
| MATRIX_ACCESS_TOKEN   | Matrix access token              | J5FvmCXf18m8qK8K...                                   |
| MATRIX_USER_WHITELIST | Whitelisted users are unaffected | @user1:matrix.org,@user2:matrix.org,@user3:matrix.org |

The app is Heroku-compatible, so it's as easy as `git push prod master` where `prod` is a Heroku-compatible remote.

## License

WTFPL. See the `LICENSE` file for more info.
